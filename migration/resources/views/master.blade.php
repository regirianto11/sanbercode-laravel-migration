@include('layout.header')


<!-- Site wrapper -->

  <!-- Navbar -->
      @include('layout.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('layout.sidebar')
    @include('layout.content-header')
  @include('layout.main')
  
  @include('layout.footer')
  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
<!-- ./wrapper -->

@include('layout.end')
