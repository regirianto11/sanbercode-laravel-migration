@extends('master')
  @section('tittle','Form Tambah')
@section('content')
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Form Tambah</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form role="form" action="/cast" method="Post">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="nama-cast">Nama Pemain</label>
            <input type="text" class="form-control" id="nama-pemain" name="nama" placeholder="Masukan Nama pemain">
            @error('nama')
                <div class="alert alert-danger mt-1">{{ $message }}</div>
            @enderror

          </div>
          <div class="form-group">
            <label for="umur">Umur </label>
            <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukan Umur Pemain">
            @error('umur')
                <div class="alert alert-danger mt-1 ">{{ $message }}</div>
            @enderror

          </div>
          <div class="form-group">
            <label>Bio Pemain</label>
            <textarea class="form-control" rows="3" name="bio" placeholder="Masukan Bio Pemain"></textarea>
          </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
@endsection