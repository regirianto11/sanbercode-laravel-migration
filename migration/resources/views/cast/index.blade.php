@extends('../master')

  @section('tittle','Daftar Cast')
@section('content')
<div class="card">
      <div class="card-header d-flex justify-content-between">
        <a href="/cast/create" class="btn btn-primary card-title">Tambah Data Cast</a>

      </div>
      <!-- /.card-header -->
      <div class="card-body">
        @if(session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{session('success')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>id</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($casts as $key => $cast )
            <tr>
              <td>{{$key+1}}</td>
              <td>{{$cast->nama}}</td>
              <td>{{$cast->umur}}</td>
              <td>{{$cast->bio}}</td>
              <td class="d-flex">
                <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm mr-2">Info</a>
                <a href="/cast/{{$cast->id}}/edit" class="btn btn-success btn-sm mr-2">Edit</a>
                <form action="/cast/{{$cast->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>id</th>
              <th>Nama</th>
              <th>Umur(s)</th>
              <th>Bio</th>
              <th>Action</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
@endsection

@push('script')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
   $(function () {
    $('#example1').DataTable();
  });
</script>
@endpush